import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-receipt-page',
  templateUrl: './receipt-page.component.html',
  styleUrls: ['./receipt-page.component.scss']
})
export class ReceiptPageComponent implements OnInit {
  @Input('selectedProductList') selectedProductList: [];
  @Input('finalQuentity') finalQuentity: any;
  @Input('finalTotalPrice') finalTotalPrice : any;
  @Input('vatPercantage') vatPercantage : any;
  @Input('discountPercantage') discountPercantage : any;
  @Output('closemodal') closemodal: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  valuechange() {
    console.log("record date", this.closemodal);
    this.closemodal.emit(this.closemodal);
  }

}
