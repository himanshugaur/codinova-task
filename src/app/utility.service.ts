import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {
  productInfo = [
    {
      "id": "1",
      "name": "comuter",
      "price": "130",
      "category": "computers",
      "description": "",
      "image": "../assets/comuter.jpg"
    },
    {
      "id": "2",
      "name": "tie",
      "price": "46",
      "category": "Clothing",
      "description": "fashion, tie, clothes, accessory , accessoire,…",
      "image": "../assets/tie.jpeg"
    },
    {
      "id": "3",
      "name": "jacket",
      "price": "190",
      "category": "Clothing",
      "description": "winter  jacket ",
      "image": "../assets/jacket.jpeg"
    },
    {
      "id": "4",
      "name": "jacket men",
      "price": "225",
      "category": "Clothing",
      "description": "fashion  man  jacket ",
      "image": "../assets/jacket_men.jpg"
    },
    {
      "id": "5",
      "name": "grapes",
      "price": "18",
      "category": "fruits",
      "description": "food ,  leaf,  grape,s  wet,  green",
      "image": "../assets/grapes.jpeg"
    },
    {
      "id": "6",
      "name": "strawberries",
      "price": "15",
      "category": "fruits",
      "description": "healthy  red sweet  strawberries",
      "image": "../assets/strawberries.jpeg"
    },
    {
      "id": "7",
      "name": "kiwi",
      "price": "50",
      "category": "fruits",
      "description": "fruit  kiwi ",
      "image": "../assets/kiwi.jpeg"
    },
    {
      "id": "8",
      "name": "mouse",
      "price": "80",
      "category": "computers",
      "description": "apple   mouse  ",
      "image": "../assets/mouse.jpg"
    },
    {
      "id": "9",
      "name": "keyboard",
      "price": "80",
      "category": "computers",
      "description": "apple mac  keyboard",
      "image": "../assets/keyboard.jpg"
    },
    {
      "id": "10",
      "name": "headphone",
      "price": "120",
      "category": "computers",
      "description": "music headphone",
      "image": "../assets/headphone.jpg"
    },
    {
      "id": "11",
      "name": "motherboard",
      "price": "179",
      "category": "computers",
      "description": "pc motherboard with 16 Gb RAM",
      "image": "../assets/motherboard.jpg"
    },
    {
      "id": "12",
      "name": "notebook",
      "price": "760",
      "category": "computers",
      "description": "macbook  notebook  computer",
      "image": "../assets/notebook.jpg"
    },
    {
      "id": "13",
      "name": "computer repair",
      "price": "350",
      "category": "services",
      "description": "standard computer repairing",
      "image": "../assets/computer_repair.jpeg"
    },
    {
      "id": "14",
      "name": "gift folding",
      "price": "7",
      "category": "services",
      "description": "",
      "image": "../assets/gift_folding.jpeg"
    },
    {
      "id": "15",
      "name": "Clothing",
      "price": "100",
      "category": "Clothing",
      "description": "",
      "image": "../assets/clothing.jpg"
    },
  ]
  constructor() { }
}
