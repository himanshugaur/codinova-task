import { Component, OnInit } from '@angular/core';
// import "product" from "./product.json";
import { UtilityService } from "../utility.service";
import { isNgTemplate } from '@angular/compiler';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {
  productData = [];
  selectedProductList = [];
  img = "../assets/comuter.jpg"
  productForm: FormGroup;
  finalQuentity: number = 0;
  finalVat: number = 0;
  finalDiscount: number = 0;
  show_modal: boolean = false;
  constructor(public utility: UtilityService, private formbuilder: FormBuilder) {
    this.productData = this.utility.productInfo;
  }

  ngOnInit() {
    console.log("product-data=>", this.productData)
    this.initForm();
    this.detectFormValueChange();
  }

  initForm() {
    this.productForm = this.formbuilder.group({
      subtotal: [
        '',
        [Validators.required]
      ],
      vat: [
        10,
        [Validators.required]
      ],
      discount: [
        10,
        [Validators.required]
      ],
      total: [
        '',
        [Validators.required]
      ]
    })
  }

  mouseEnter(item, index) {
    this.makeHoverFalse();
    this.productData[index].showInfo = true
  }

  mouseLeave(item, index) {
    this.makeHoverFalse();
  }

  makeHoverFalse() {
    this.productData.map((product) => {
      product.showInfo = false
    })
  }

  selectProduct(item, index) {
    if (this.selectedProductList.length == 0) {
      this.productSelectedFirstTime(item, index);
    } else {
      if (!item.isSelected) {
        this.productSelectedFirstTime(item, index);
      } else {
        item.quantity++;
        item.totalPrice = parseInt(item.totalPrice) + parseInt(item.price);
      }
    }
    console.log("total-selected-product", this.selectedProductList);
    this.calculateProductValue();
  }

  productSelectedFirstTime(item, index) {
    item.quantity = 1
    item.totalPrice = item.price;
    item.isSelected = true;
    this.selectedProductList.push(item);
  }

  removeProduct(index, item) {
    this.selectedProductList.splice(index, 1);
    this.productData = this.productData.map((product) => {
      if (product.id == item.id) {
        delete product.quantity;
        delete product.totalPrice
        product.isSelected = false;
      }
      return product;
    })
    console.log("main thing=>", this.productData);
    this.calculateProductValue();
  }

  removeQantityofProduct(item, index) {
    if (item.quantity == 1) {
      this.selectedProductList.splice(index, 1)
    } else {
      item.quantity--;
      item.totalPrice = parseInt(item.totalPrice) - parseInt(item.price);
    }
    this.calculateProductValue()
  }


  addQantityofProduct(item, index) {
    item.quantity++;
    item.totalPrice = parseInt(item.totalPrice) + parseInt(item.price);
    this.calculateProductValue()
  }

  pocessSale() {
    console.log("form-value", this.productForm);
    if (this.productForm.valid) {
      console.log("prdocut form is valid", this.productForm)
      this.show_modal = true;
    } else {
      alert("please select any product from left hand side")
    }
  }

  cancelSale() {
    this.closeModal();
  }

  inputNumbers(ev) {
    console.log("event==>", ev);
    if (ev.keyCode >= 48 && ev.keyCode <= 57) {
      return true;
    }
    return false;
  }

  calculateProductValue() {
    let finalSubTotal = 0;
    this.finalQuentity = 0;
    this.finalVat = 0;
    this.finalDiscount = 0;
    let finalTotalPrice = 0;
    for (let i = 0; i < this.selectedProductList.length; i++) {
      finalSubTotal = finalSubTotal + parseInt(this.selectedProductList[i].totalPrice);
      this.finalQuentity = this.finalQuentity + parseInt(this.selectedProductList[i].quantity);
    }
    this.finalVat = Math.floor((finalSubTotal * this.productForm.controls['vat'].value) / 100);
    this.finalDiscount = Math.floor((finalSubTotal * this.productForm.controls['discount'].value) / 100);
    console.log("vat we get", this.finalVat);
    finalTotalPrice = finalSubTotal + this.finalVat + this.finalDiscount;
    this.productForm.controls['subtotal'].setValue(finalSubTotal);
    this.productForm.controls['total'].setValue(finalTotalPrice);
  }

  closeModal() {
    this.show_modal = false;
    this.productForm.reset();
    this.selectedProductList = [];
    this.finalQuentity = 0;
    this.finalVat = 0;
    this.finalDiscount = 0;
    this.productData.forEach((product)=> {
     product.isSelected = false;
    })
    this.productForm.controls['subtotal'].setValue('');
    console.log("form-val", this.productForm.value)
  }

  detectFormValueChange() {
    this.productForm.controls['vat'].valueChanges.subscribe((value: any) => {
      console.log("subscribe-event");
      this.calculateProductValue();
    });

    this.productForm.controls['discount'].valueChanges.subscribe((value: any) => {
      console.log("subscribe-event");
      this.calculateProductValue();
    });
  }
}
